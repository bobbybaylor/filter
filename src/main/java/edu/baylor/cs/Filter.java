package edu.baylor.cs;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 * Simple class that loads {@link Filter#FILE_INPUT} 
 * and converts the comma delimiter to semicolon printed to {@link Filter#FILE_OUTPUT_SEMICOLON} 
 * 
 * 
 * 1. Consider the functionality in {@link Filter#main}, divide it to two non-static public methods
 * 	  {@link Filter#loader} that loads the file (path/file as parameter) into a collection of collections (two dimensions - rows/cols); 
 *    {@link Filter#saver} that saves the collection passed as parameter to the file passed as a second parameter
 *      Saver has columns divided via semi-colons
 *    	Loader can throw FilterException if data are malformatted
 *      Loader can skip the file header
 * 	
 * 2. Create new class {@link DTO} (data trasfer object)
 *    It has 5 fields:
 *    	private Integer year;
 * 		private String team;
 * 		private String peague;
 * 		private String player;
 * 		private Long salary;
 * 		make getters/setters/equals/hashcoade
 * 		it implements a method toString that returns a string of columns is expected order divided by semi-colon
 * 		if any column is null print empty string, use ternary operator 
 * 		
 * 3. Modify collection from step 1 to load the data from the file to the DTOs, a DTO instance is one row
 * 
 * 4.1 Introduce a static generic (parametrizable) method contentMatcher that matches the content of two collection<T> and returns true or false
 * 		Remember not a generic class, only generic method
 * 		Use collection operations to make it easy, no loop, no iteration, no recursion
 * 		Remember to clone your collection
 * 
 * 4.2 Introduce a static method salaryCheck that makes sure that each element in the collection<DTO> has a salary > 0
 * 		Streams can use method allMatch
 * 		Does it return true or false?
 * 
 * 5. Make a JUnit 5 to test that
 * 		the loader method loads expected number of rows from know file
 * 		the loader reacts well on non-existing file
 * 		the loader throws FilterException when file does not follow the expected format of 5 fields (int,String,...int)
 *      the saver saves properly one line if we pass a single line collection (explicitly check file content in the test)
 *      test the contentMatcher for match and non-match
 *      test the salaryCheck for match and non-match
 *      remember each test is individual no 
 *       
 * 5. Create a new Class called {@link Strategy} that has a delegate field the {@link Filter} instantiated in constructor,
 * 		it has one non-static protected method called {@link Strategy#apply} that is empty by default and takes List<DTO> list as parameter and returns such a list
 * 		it has one more non-static protected method called {@link Strategy#getPath} that return the filePath to the output file (just return a string)
 *      it has one non-static final public method called {@link Strategy#perform}, and by default it calls 
 *      {@link Filter#loader}{@link Strategy#apply}{@link Filter#saver}
 *      Call this method from {@link Filter#main}
 * 
 * 6. Create 3 specialized types of {@link Strategy} (e.g. MilionOrMoreStrategy). Each with distinct name and content of {@link Strategy#report(Collection<DTO>)}.
 *      In {@link Strategy#apply} it uses the Collection parameter and Streams to get (3 types)
 * 		1. filtered only rows with salaries over 1 million
 *      2. content with total paid USD per a year, for the salaries of players in the sheet
 *      		use Streams to get a map and then create new DTO collection that only has year and salary sum (the rest empty)
 *      		iterate over the map to get DTO collection 
 *      3. content with total paid USD per a team no matter the year, for the salaries of players in the sheet
 *      		use Streams to get a map and then create new DTO collection that only has team and salary sum (the rest empty)
 *          iterate over the map to get DTO collection 
 *      Each special strategy overrides the getPath method and uses file name as indicated in {@link Filter} constants
 *      Call each special type of strategies in {@link Filter#main} so in total it calls 4 strategy types and generates 4 files
 *      
 * 7. Write JavaDOC for your creation, for each class and method, well comment and format your code
 * 
 * 
 * @author cerny
 *
 */
public class Filter {
	final public static String FILE_INPUT = "./Salaries.csv";
	final public static String FILE_OUTPUT_SEMICOLON = "./outputSalaries.csv";
	final public static String FILE_OUTPUT_MILLION_OR_MORE = "./outputMilionSalaries.csv";
	final public static String FILE_OUTPUT_TOTAL_PER_YEAR = "./outputYearTotalSalaries.csv";
	final public static String FILE_OUTPUT_TOTAL_PER_TEAM = "./outputTeamSalaries.csv";
	private final static Logger LOGGER = Logger.getLogger(Filter.class.getName());
	
	public static void main(String[] args) {
		
		File fileIn = new File(FILE_INPUT);

		Scanner scanner = null;
		PrintWriter out = null;

		try {
			// init input
			scanner = new Scanner(fileIn);
			LOGGER.info("yearID\tteamID\tlgID\tplayerID\tsalary");
			
			// init output
			out = new PrintWriter(FILE_OUTPUT_SEMICOLON);
			
			while (scanner.hasNext()) {
				String line = scanner.next();
				String[] values = line.split(",");
				// this adds the currently parsed line to the 2-dimensional string array
				for (int i = 0; i< values.length;i++) {
					out.write(values[i]);
					if (i < values.length-1) {
						out.write(";");
					} else {
						out.println();
					}
				}
				
			}

		} catch (FileNotFoundException e) {
			LOGGER.severe("Exception: "+e.getMessage());
		} finally {
			if(out != null) {
				out.close();
			}
			if(scanner != null) {
				scanner.close();
			}
		}

		
	}
}
